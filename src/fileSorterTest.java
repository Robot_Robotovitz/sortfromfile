import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class fileSorterTest {

    //В идеале, тут нужно поюзать JUnit, или что-нибудь
    //Но для проверки этого хватит

    public static void main(String[] args) {
        List<Integer> numSetAscending = new ArrayList<>();
        for (int i = 0; i < 21; i++){
            numSetAscending.add(i);
        }

        List<Integer> numSetDescending = new ArrayList<>(numSetAscending);
        numSetDescending.sort(Comparator.reverseOrder());


        List<Integer> testList = new ArrayList<>(numSetAscending);
        Collections.shuffle(testList);


        String file = "test.txt";

//        try(FileOutputStream outStream = new FileOutputStream(file)) {
//            for(Integer i : testList){
//                outStream.write( (i + " ,").getBytes());
//
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        try(FileOutputStream outStream = new FileOutputStream(file)) {
            for(Integer i : testList){
                if(i==10) outStream.write((i + ",\n").getBytes());
                else if (i == 20)  outStream.write((","+ i + "\n").getBytes());
                else outStream.write( (i + " , ").getBytes());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        List<Integer> list = null;


        try {
            list = fileSorter.sortFromFileAscending(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!list.equals(numSetAscending)) {
            Main.printf("lists are not equal after sortFromFileAscending! ");
            Main.printf("ER: " + numSetAscending.toString() );
            Main.printf("AR: " + list.toString() );
        }


        try {
            list = fileSorter.sortFromFileDescending(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!list.equals(numSetDescending)) {
            Main.printf("lists are not equal after sortFromFileDescending! ");

            Main.printf("ER: " + numSetDescending.toString() );
            Main.printf("AR: " + list.toString() );
        }

    }

}