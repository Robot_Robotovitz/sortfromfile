import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    static void printf(String str) {
        System.out.println(str);
    }

    static void printf(String str, Exception e) {
        System.out.println(str);
        e.printStackTrace();
    }

    public static void main(String[] args) {

       String file = "test.txt";//args[0];
        List<Integer> list = null;

        try {
            list = fileSorter.sortFromFileAscending(file);
        } catch (IOException e) {
            printf("Something wrong with the file! " , e);
            System.exit(1);
        }
        printf(list.toString());


        try {
            list = fileSorter.sortFromFileDescending(file);
        } catch (IOException e) {
            printf("Something wrong with the file! " , e);
            System.exit(1);
        }
        printf(list.toString());

    }
}
