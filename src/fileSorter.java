import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class fileSorter {
    public static List<Integer> sortFromFileAscending (String fileName) throws IOException {
        List<Integer> list = getListFromFile(fileName);
        Collections.sort(list);
        return list;
    }

    public static List<Integer> sortFromFileDescending (String fileName) throws IOException {
        List<Integer> list = getListFromFile(fileName);
        list.sort(Collections.reverseOrder());
        return list;
    }

    private static List<Integer> getListFromFile(String fileName) throws IOException {
        List<Integer> list = new ArrayList<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(fileName)) ){

            while (reader.ready())
            {
                String data = reader.readLine();
                String[] values = data.split(" ?, ?");//ноль или более пробелов, запятая, ноль или более пробелов
                for (String str : values) {
                    str = str.trim();
                    if(!str.isEmpty()) list.add(Integer.valueOf(str.trim()));
                }

            }
        }

        return list;
    }

}
